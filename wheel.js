function draw() {
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");

  ctx.font = "20px Consolas";
  ctx.textAlign = 'center';

  var centerX = canvas.width / 2
  var centerY = canvas.height / 2

  var categories = ['sport', 'IT', 'art', 'music', 'dance', 'diy', 'gaming', 'fishing']
  var stats = [2, 5, 8, 6, 8, 9, 5, 4]
  
  var colors = ['#4CAF50', '#00BCD4', '#E91E63', '#FFC107', '#9E9E9E', '#CDDC39', '#18FFFF', '#F44336', '#FFF59D', '#6D4C41'];
  var angles = Math.PI * 2 / categories.length

  var offset = 10;
  var beginAngle = 0;
  var endAngle = 0;
  var offsetX, offsetY, medianAngle;

  ctx.beginPath();
  ctx.fillStyle = '#FDFDFD';
  ctx.moveTo(centerX, centerY);
  ctx.arc(centerX, centerY, 15 * 10 + offset, 0, Math.PI * 2);
  ctx.fill();
  
  for (var i = 0; i < categories.length; i++) {
    beginAngle = endAngle;
    endAngle = endAngle + angles;
    medianAngle = (endAngle + beginAngle) / 2;

    offsetX = Math.cos(medianAngle) * offset;
    offsetY = Math.sin(medianAngle) * offset;

    ctx.fillStyle = '#FAFAFA';
    ctx.beginPath();
    ctx.moveTo(centerX + offsetX, centerY + offsetY);
    ctx.arc(centerX + offsetX, centerY + offsetY, 15 * 10, beginAngle, endAngle);
    ctx.fill();

    ctx.fillStyle = colors[i % colors.length];
    ctx.beginPath();
    ctx.moveTo(centerX + offsetX, centerY + offsetY);
    ctx.arc(centerX + offsetX, centerY + offsetY, 15 * stats[i], beginAngle, endAngle);
    ctx.fill();

    ctx.save();
    ctx.translate(centerX, centerY);
    ctx.rotate(medianAngle);
    ctx.fillStyle = colors[i + 1 % colors.length];
    ctx.fillText(categories[i], 150 / 2, 0);
    ctx.restore()
  }
}